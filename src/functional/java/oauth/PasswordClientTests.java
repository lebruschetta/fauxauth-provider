package oauth;

import oauth.clients.ClientStore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class PasswordClientTests extends FauxauthProviderFunctionalTest {
    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void http200IsReturnedForPasswordGrantTypeAndClientIdOfWebapp() throws Exception {
        try{
            Response response = makeHttpCallUsingPasswordGrantType("webapp", "admin", "password");
            assertEquals(200, response.getStatus());
        }
        catch (RetrofitError error){
            fail("This test should have passed: " +error.getResponse().getBody());
        }
    }

    @Test
    public void http400IsReturnedForPasswordGrantTypeWhenUsernameNotFound() throws Exception {
        testBadCredentialsWithClient("webapp", "notarealadmin", "password");
    }

    @Test
    public void http400IsReturnedForPasswordGrantTypeWhenPasswordIsBad() throws Exception {
        testBadCredentialsWithClient("webapp", "admin", "badpass");
    }


    private void testBadCredentialsWithClient(String client, String username, String password) {
        try {
            fauxAuthClient.fetchTokenWithPasswordGrantWithMapResponse(
                    getEncodedAuthHeader(ClientStore.clients.get(client)),
                    username,
                    password,
                    "password");
            fail("authentication shouldve failed");
        } catch(RetrofitError e) {
            assertEquals(400, e.getResponse().getStatus());
        }
    }

    private Response makeHttpCallUsingPasswordGrantType(String client, String username, String password) {
        return fauxAuthClient.fetchTokenWithPasswordGrant(getEncodedAuthHeader(ClientStore.clients.get(client)), username, password, ClientStore.GRANT_TYPE_PASSWORD);
    }
}
