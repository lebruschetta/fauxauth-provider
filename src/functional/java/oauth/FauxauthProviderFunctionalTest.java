package oauth;

import oauth.clients.FauxAuthClient;
import oauth.config.FunctionalTestConfig;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import retrofit.client.Response;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = FunctionalTestConfig.class)
@WebIntegrationTest
public class FauxauthProviderFunctionalTest {

    @Autowired FauxAuthClient fauxAuthClient;

    @Test
    public void itShouldJUNIT() throws Exception {

    }

    public void assertHttp200(Response response) {
        assertNotNull(response);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }


    public String getEncodedAuthHeader(BaseClientDetails baseClientDetails) {
        System.out.println("--> " + baseClientDetails.getClientId() + ":" + baseClientDetails.getClientSecret());
        return "Basic " + new String(Base64.encodeBase64((baseClientDetails.getClientId() + ":" + baseClientDetails.getClientSecret()).getBytes()));
    }

    public void verifyValidToken(String token, String username) {
        Map<String, Object> response = fauxAuthClient.checkTokenWithMapResponse(token);
        assertEquals(username, response.get("user_name"));
    }
}
