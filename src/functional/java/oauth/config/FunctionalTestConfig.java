package oauth.config;

import oauth.FauxAuthProvider;
import oauth.clients.FauxAuthClient;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit.RestAdapter;
import retrofit.converter.JacksonConverter;

@Configuration
@EnableAutoConfiguration
public class FunctionalTestConfig extends FauxAuthProvider {

    @Bean
    public RestAdapter fauxAuthRestAdapter() {
        return new RestAdapter.Builder()
                .setConverter(new JacksonConverter())
                .setEndpoint("http://localhost:8005/fauxauth-provider")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
    }

    @Bean
    public FauxAuthClient fauxAuthClient(RestAdapter copRestAdapter) {
        return copRestAdapter.create(FauxAuthClient.class);
    }

}