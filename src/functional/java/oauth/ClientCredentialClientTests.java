package oauth;

import oauth.clients.ClientStore;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import retrofit.RetrofitError;
import retrofit.client.Response;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ClientCredentialClientTests extends FauxauthProviderFunctionalTest {


    @Test
    public void http200IsReturnedForClientCredentialGrantTypeAndClientIdOfService() throws Exception {
        assertHttp200(makeHttpCallUsingClientCredentialGrantType(getEncodedAuthHeader(ClientStore.clients.get("service"))));
    }

    @Test
    public void http401IsReturnedForClientCredentialGrantTypeWhenAuthHeaderIsBad() throws Exception {
        try {
            makeHttpCallUsingClientCredentialGrantType("ThisIsABadAuthorizationHeader");
            fail("HTTP 401 error should have been thrown...");
        } catch (RetrofitError error) {
            assertEquals(HttpStatus.UNAUTHORIZED.value(), error.getResponse().getStatus());
        }
    }

    public Response makeHttpCallUsingClientCredentialGrantType(String authorizationHeader) {
        return fauxAuthClient.fetchToken(authorizationHeader, ClientStore.GRANT_TYPE_CLIENT_CREDENTIALS);
    }
}
