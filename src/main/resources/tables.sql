create table oauth_access_token (
  token_id VARCHAR(256),
  token LONGVARBINARY(2000),
  authentication_id VARCHAR(256) PRIMARY KEY,
  user_name VARCHAR(256),
  client_id VARCHAR(256),
  authentication LONGVARBINARY(4000),
  refresh_token VARCHAR(256)
);

create table oauth_refresh_token (
  token_id VARCHAR(256),
  token LONGVARBINARY(2000),
  authentication LONGVARBINARY(4000)
);
