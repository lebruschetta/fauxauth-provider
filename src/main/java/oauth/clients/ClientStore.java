package oauth.clients;

import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.util.HashMap;
import java.util.Map;

public class ClientStore {

    public static final int ONE_WEEK = 7;
    public static final String GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";
    public static final String GRANT_TYPE_PASSWORD = "password";
    public static Map<String, BaseClientDetails> clients = new HashMap<>();
    public static Map<String, Integer> tokenExpiration = new HashMap<>();

    static {
        initClients();
    }

    private static void initClients() {
        BaseClientDetails service = new BaseClientDetails("service", null, "read,write", GRANT_TYPE_CLIENT_CREDENTIALS, "ROLE_SERVICE");
        service.setClientSecret("c2VydmljZXNlY3JldA==");
        clients.put(Clients.SERVICE.toValue(), service);
        tokenExpiration.put(Clients.SERVICE.toValue(), ONE_WEEK);

        BaseClientDetails webapp = new BaseClientDetails("webapp", null, "read,write", GRANT_TYPE_PASSWORD, "ROLE_WEBAPP");
        webapp.setClientSecret("d2ViYXBwc2VjcmV0");
        clients.put(Clients.WEBAPP.toValue(), webapp);
        tokenExpiration.put(Clients.WEBAPP.toValue(), ONE_WEEK);

        BaseClientDetails mobile = new BaseClientDetails("mobile", null, "read,write", GRANT_TYPE_PASSWORD, "ROLE_MOBILE");
        mobile.setClientSecret("bW9iaWxlc2VjcmV0");
        clients.put(Clients.MOBILE.toValue(), mobile);
        tokenExpiration.put(Clients.MOBILE.toValue(), ONE_WEEK);

        BaseClientDetails proxy = new BaseClientDetails("proxy", null, "read,write", GRANT_TYPE_CLIENT_CREDENTIALS, "ROLE_PROXY");
        proxy.setClientSecret("totesasecret");
        clients.put(Clients.PROXY.toValue(), proxy);
        tokenExpiration.put(Clients.PROXY.toValue(), ONE_WEEK);
    }
}
