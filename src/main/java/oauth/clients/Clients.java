package oauth.clients;

public enum Clients {
    SERVICE("service"),
    WEBAPP("webapp"),
    MOBILE("mobile"),
    PROXY("proxy");

    String value;

    Clients(String value) {
        this.value = value;
    }

    public String toValue() {
        return this.value;
    }
}
