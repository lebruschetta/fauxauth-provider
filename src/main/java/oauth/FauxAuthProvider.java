package oauth;

import oauth.authentication.FauxauthTokenEnhancer;
import oauth.authentication.FauxauthTokenGranter;
import oauth.authentication.provider.ActiveDirectoryAuthenticationProvider;
import oauth.authentication.provider.GenericAccountServiceAuthenticationProvider;
import oauth.authentication.service.ActiveDirectoryAuthenticationService;
import oauth.clients.ClientStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.client.InMemoryClientDetailsService;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.*;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.NoInitialContextException;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
public class FauxAuthProvider {

    public static void main(String[] args) {
        SpringApplication.run(FauxAuthProvider.class, args);
    }

    @Bean
    public DataSource dataSource() {

        try {
            Context context = new InitialContext();
            return (DataSource) context.lookup("java:comp/env/jdbc/some_jndi_connection");
        } catch (NoInitialContextException noContextException) {
            return new EmbeddedDatabaseBuilder()
                    .setType(EmbeddedDatabaseType.H2)
                    .addScript("classpath:tables.sql")
                    .setName(UUID.randomUUID().toString())
                    .build();

        } catch(NamingException nameException) {

        }
        return null;
    }

    /*
    Client details are stored in memory in the ClientStore
     */
    @Bean
    public ClientDetailsService clientDetailsService() {
        InMemoryClientDetailsService service = new InMemoryClientDetailsService();
        service.setClientDetailsStore(ClientStore.clients);
        return service;
    }

    /*
    Wire the providers/services intending to user username/password authentication
     */
    @Bean
    public AuthenticationProvider genericAccountServiceAuthenticationProvider() { return new GenericAccountServiceAuthenticationProvider(); }

    @Bean
    public ActiveDirectoryAuthenticationService activeDirectoryAuthenticationService() { return new ActiveDirectoryAuthenticationService(); }

    @Bean
    public AuthenticationProvider activeDirectoryAuthenticationProvider() { return new ActiveDirectoryAuthenticationProvider(); }


    /*
    Augments token expiration
     */
    @Bean
    public TokenEnhancer tokenEnhancer() {
        return new FauxauthTokenEnhancer();
    }

    /*
    Spring SPI for inject all our custom token logic
     */
    @Bean
    public AuthorizationServerTokenServices authorizationServerTokenServices(TokenStore tokenStore, TokenEnhancer tokenEnhancer) {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore);
        defaultTokenServices.setTokenEnhancer(tokenEnhancer);
        return defaultTokenServices;
    }

    /*
    Built in spring implementation of storing access tokens in the database
     */
    @Bean
    public TokenStore tokenStore(DataSource dataSource) {return new JdbcTokenStore(dataSource);
    }

    /*
    Injecting these guys allows us to enable password grants
     */
    @Bean
    AuthenticationManager authenticationManager(ObjectPostProcessor<Object> objectPostProcessor,
                                                AuthenticationProvider genericAccountServiceAuthenticationProvider,
                                                AuthenticationProvider activeDirectoryAuthenticationProvider
    ) throws Exception {

        AuthenticationManagerBuilder authenticationManagerBuilder = new AuthenticationManagerBuilder(objectPostProcessor);

        //adding our two distinct password grant auth providers...
        authenticationManagerBuilder.authenticationProvider(genericAccountServiceAuthenticationProvider);
        authenticationManagerBuilder.authenticationProvider(activeDirectoryAuthenticationProvider);

        return authenticationManagerBuilder.getOrBuild();
    }

    @Bean
    AccessTokenConverter accessTokenConverter() {
        return new DefaultAccessTokenConverter();
    }

    @Bean
    OAuth2RequestFactory requestFactory(ClientDetailsService clientDetailsService) {
        return new DefaultOAuth2RequestFactory(clientDetailsService);
    }

    @Bean
    FauxauthTokenGranter fauxauthTokenGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        return new FauxauthTokenGranter(authenticationManager, tokenServices, clientDetailsService, requestFactory);
    }

    @Bean ClientCredentialsTokenGranter clientCredentialsTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory){
        return new ClientCredentialsTokenGranter(tokenServices, clientDetailsService, requestFactory);
    }

    @Configuration
    @EnableAuthorizationServer
    static class OAuth2AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Autowired
        private AuthenticationManager authenticationManager;

        @Autowired
        private ClientDetailsService clientDetailsService;

        @Autowired
        private TokenStore tokenStore;

        @Autowired
        private AuthorizationServerTokenServices authorizationServerTokenServices;

        @Autowired
        private TokenEnhancer tokenEnhancer;

        @Autowired AccessTokenConverter accessTokenConverter;
        @Autowired FauxauthTokenGranter fauxauthTokenGranter;
        @Autowired ClientCredentialsTokenGranter clientCredentialsTokenGranter;

        /*
        Controls our available
         */
        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.withClientDetails(clientDetailsService);
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.clientDetailsService(clientDetailsService);
            endpoints.tokenStore(tokenStore);
            endpoints.tokenServices(authorizationServerTokenServices);
            endpoints.authenticationManager(authenticationManager);
            endpoints.tokenEnhancer(tokenEnhancer);
            endpoints.accessTokenConverter(accessTokenConverter);

            List<TokenGranter> tokenGranters = new ArrayList<TokenGranter>();
            tokenGranters.add(fauxauthTokenGranter);
            tokenGranters.add(clientCredentialsTokenGranter);
            endpoints.tokenGranter(new CompositeTokenGranter(tokenGranters));
        }

        @Override
        public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
            oauthServer.checkTokenAccess("permitAll()");
        }
    }
}