package oauth.authentication.service;


import java.util.HashMap;
import java.util.Map;

/*
In the real world, this would set allowed groups and perform other LDAPy tasks
such as check password, etc...
 */
public class ActiveDirectoryAuthenticationService {

    private static Map<String, String> totesSecureMap= new HashMap();

    static{
        totesSecureMap.put("admin", "password");
        totesSecureMap.put("user", "wordpass");
    }

    public boolean checkPassword(String username, String password){
        boolean authenticated = false;

        if(password.equals(totesSecureMap.get(username)))
            authenticated= true;

        return authenticated;
    }
}
