package oauth.authentication;

import oauth.clients.ClientStore;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

@Component
public class FauxauthTokenEnhancer implements TokenEnhancer {

    Logger logger = LoggerFactory.getLogger(getClass());

    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(accessToken);

        logger.info("Enhancing expiration date for authentication: " + authentication + " with access token of: " + accessToken);

        int expirationTimeInDays = ClientStore.tokenExpiration.get(authentication.getOAuth2Request().getClientId());
        DateTime expireDate = new DateTime().plusDays(expirationTimeInDays);
        token.setExpiration(expireDate.toDate());

        logger.info("Finished enhancing expiration date for authentication: " + authentication + " with new access token of: " + token);

        return token;
    }
}
