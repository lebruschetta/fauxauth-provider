package oauth.authentication.provider;

import oauth.authentication.service.ActiveDirectoryAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class ActiveDirectoryAuthenticationProvider implements FauxAuthenticationProvider {

    @Autowired ActiveDirectoryAuthenticationService activeDirectoryAuthenticationService;
    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Optional<UsernamePasswordAuthenticationToken> checkAuthentication(Authentication authentication) {

        if(activeDirectoryAuthenticationService.checkPassword(authentication.getPrincipal().toString(), authentication.getCredentials().toString())) {
            return Optional.of(
                new UsernamePasswordAuthenticationToken(
                    authentication.getPrincipal(),
                    authentication.getCredentials(),
                    authentication.getAuthorities())
            );
        }

        return Optional.empty();
    }

    @Override
    public List<GrantedAuthority> supportsAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_WEBAPP"), new SimpleGrantedAuthority("ROLE_MOBILE"));
    }
}
