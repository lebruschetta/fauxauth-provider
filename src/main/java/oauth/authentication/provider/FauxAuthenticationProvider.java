package oauth.authentication.provider;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;
import java.util.Optional;

public interface FauxAuthenticationProvider extends AuthenticationProvider {

    /*
    Method should authenticate a user based on a specific scheme (LDAP, proprietary user account service, etc..)
    */
    Optional<UsernamePasswordAuthenticationToken> checkAuthentication(Authentication authentication);

    /*
    Should return a list of authorities that this implementation of FauxAuthenticationProvider should
    be used for.
    */
    List<GrantedAuthority> supportsAuthorities();

    @Override
    public default Authentication authenticate(Authentication authentication) throws AuthenticationException {


        if (null == authentication.getPrincipal() ||
                null == authentication.getCredentials()) {
            throw new BadCredentialsException("Empty username or password");
        }

        if(supportsAuthorities().stream().noneMatch(x -> authentication.getAuthorities().contains(x))) {
            return null;
        }

        return checkAuthentication(authentication).orElseThrow(() -> new BadCredentialsException("Invalid username or Password"));
    }

    @Override
    public default boolean supports(Class<?> authentication) {
        return true;
    }
}
