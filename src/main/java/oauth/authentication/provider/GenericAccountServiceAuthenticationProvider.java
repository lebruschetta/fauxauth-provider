package oauth.authentication.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
/*
Generic Account i.e. if you have a hand-rolled user/pass service of sorts...
These providers would call said service, in our case OneAccount.
 */
@Component
public class GenericAccountServiceAuthenticationProvider implements FauxAuthenticationProvider {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Optional<UsernamePasswordAuthenticationToken> checkAuthentication(Authentication authentication) {
        try {
            //In the real world, we would call our hand-rolled service here.
            //throwing the exception to illustrate a failure, could be something like
            //a hand-rolled missing account exception etc...
            throw new NoSuchMethodError();
        } catch (Exception e) {
            return Optional.empty();
        }

        //we would have some details to pass back if authentication was successful
//        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
//                new UsernamePasswordAuthenticationToken("a unique identifier for this authorized user, such as an account id",
//                                                        authentication.getCredentials(),
//                                                        authentication.getAuthorities());
//
//        return Optional.of(usernamePasswordAuthenticationToken);
    }

    @Override
    public List<GrantedAuthority> supportsAuthorities() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_DOESNTEXIST"));
    }

}
