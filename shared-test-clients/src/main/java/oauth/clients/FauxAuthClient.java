package oauth.clients;

import retrofit.client.Response;
import retrofit.http.*;

import java.util.Map;

public interface FauxAuthClient {

    @POST("/oauth/token")
    Response fetchToken(@Header("Authorization") String authorization,  @Query("grant_type") String grantType);

    @POST("/oauth/token")
    Map<String, String> fetchTokenWithMapResponse(@Header("Authorization") String authorization,  @Query("grant_type") String grantType);

    @FormUrlEncoded
    @POST("/oauth/token")
    Response fetchTokenWithPasswordGrant(@Header("Authorization") String authorization,
                                         @Field("username") String username,
                                         @Field("password") String password,
                                         @Query("grant_type") String grantType);

    @FormUrlEncoded
    @POST("/oauth/token")
    Map<String, String> fetchTokenWithPasswordGrantWithMapResponse(@Header("Authorization") String authorization,
                                                                   @Field("username") String username,
                                                                   @Field("password") String password,
                                                                   @Query("grant_type") String grantType);

    @GET("/oauth/check_token")
    Response checkToken(@Query("token") String token);

    @GET("/oauth/check_token")
    Map<String, Object> checkTokenWithMapResponse(@Query("token") String token);
}
